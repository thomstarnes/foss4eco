# Conference Name:

 * FOSS4G-Env
 * enviroFOSS ("enviro" to me tends to encompass  
 * ecoFOSS
 * etc...

* FOSS4nature (bit longer but sounds very pro green issues/wildlife etc!)
* FOSS4bio
* FOSS4science

   check if anything similar exists, domain name availability

# Conference Committee:

* Liz Scott
* Barry Rowlingson (Lancaster Uni)
* Andy Murdock (APM Geo) 
* Thomas Starnes (RSPB)

# Conference Scope:

 Ecology? Conservation? Environmental Science
 
 * I would leave out the environmental science bit as to me this covers a far wider scope - contaminated land/water, pollution control etc

 UK? Global?
 
  * advertise within UK but all welcome

 Only geo- or all open source? "All open source" might expand the
field too much unless we focus on software rather than case
studies. Not keen.

 * I'd agree, open source geo, with a special mention for open geodata

# Topics:

Some thoughts about the types of talks we would like to see. This is
a very informal classification intended as a guide. A "Fantasy Conference"
sample agenda is included later in this document for more inspiration.

## Science Report: story about some science done using open-source.

 A Science Report's conclusion should be about the environment, rather than
the software. The software used should mostly be open source.

## Software Development: story about developing code for general eco-applications.

 A Software Development talk should have the rough form of a problem definition, 
an outline of current solutions, how the new software was designed, and how it 
fixes the problem. The conclusion should hopefully be a URL for downloading!

## Infrastructure: how an individual or team has used open-source tools for the win.

Infrastructure talks will focus on the wider usage of open-source by an individual or
within an organisation. A single scientific conclusion isn't needed, or this would
be a Science Report talk.



# Conference Format:

 * How big? 50? 100? more?
 * Where? University of Poppleton?
  * Thomas Starnes suggested the brand new David Attenborough building at the Cambridge Conservation centre, which is a hub for conservation organisations including the likes of RSPB, BTO etc
 A quick look at their website indicates they have work space for over a hundred people from different conservation organisations.
 The centre has meeting rooms, lecture theatre for 120-odd and networking space.
 Worth a look!
 
 *  When? January, Easter, Summer? Check clashes with GISRUK, OsGEO events
  * Probably better out of fieldwork season if we want ecologists who are also field workers. Winter better!
 
 Two days? One day of workshops, one day of presentations?
  * Start with one day? perhaps with talks all day and some workshops to choose to attend too.

 Single stream everywhere? Think multi-stream workshops makes sense,
multiple presentations less so since they should be more general
interest, and maybe 3x16 person workshops in parallel and 1x50 person
presentation thread works well? Perhaps if we aim for 100 people then
run twice the streams.

 * Offer a mix of techie stuff plus higher level / inspirational talks.
 * Fewer attendees are likely to be coders (or consider themselves "proper coders") than at Foss4G, so I would steer more towards power users with a bit of coding if you dare as the techie end of things.



# Fantasy Conference Agenda:

## Workshops

 * Workshop: Computing Habitat Suitability Maps in R
 * Workshop: Mapping Bird Migration in QGIS and The Web
 * Workshop: Using GRASS for Land Use Computation
 * Workshop: Get Away From Excel: Putting Your Data in a Spatial Database
 * Workshop: Drones for wildlife surveying (hands-on workshop)
 * Workshop: How to Include Open Source in your Research Grants

* Workshop/Presentation: Using CartoDB's Torque function to make animated maps of GPS tagged gulls

## Presentations

 * Presentation: Real-time Mapping of Badgers
 * Presentation: Lidar Reconstruction of Forest Nesting Sites
 * Presentation: Coastal Feature Cartography in QGIS
 * Presentation: Using R to Map Otter Distribution Changes on Mull
 * Presentation: A QGIS plugin for GPS tracker smoothing
 * Presentation: Migrating from proprietary GIS to Open Source in Our Lab
 * Presentation: Citizen Science Data Collection in the Cloud
 * Presentation: Toad habitat suitability and climate change

* Presentation: Regression modelling in R to map populations
* Presentation: Using Lidar to map sun and shade for reptile distribution mapping
* Presentation: The NBN Gateway
* Presentation: Using web mapping for effective scicom (ie an open source alternative to Story Maps)


## Others

 * Lightning Talks
 * Posters 
  * definitely - more of an academic feel than FOSS4G, plenty of FOSS users will be academics
 * Unconference 
  * need to see how well known this format is in the conservation sector
 * Panel Discussion

# Finance

 * Support from OSGEO-UK funds
 * Minimal attendance fee
 * Sponsors? 
  * open source sw support companies, maybe companies who supply technical field equipment for recording (eg NHBS)








